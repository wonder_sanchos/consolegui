﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MenuDemo
{
    public enum ButtonType
    {
        Back,
        ToTop,
        Exit
    }
    public class Button
    {
        public ButtonType _buttonType;
        public string ButtonName { get; private set; }


        public Button(ButtonType type, string name)
        {
            _buttonType = type;
            ButtonName = name;
        }

    }
}
