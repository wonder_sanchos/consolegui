﻿using System;
using static System.Console;

namespace MenuDemo
{
    class Program
    {
        //Container Current;
        static void Main(string[] args)
        {
            //Container main = new Container("Play");
            //Container galaxies = new Container("Galaxies");
            //Container stars = new Container("Stars");
            //Container planets = new Container("Planets");
            //Container sattelites = new Container("Satellites");

            //while (true)
            //{ //view
            //click
            //action
            //}

            //main.Add("Galaxies");

            //WriteLine($"Index of galaxies menu: {main.Next.Index}");
            //main.Next.Add("Stars");
            //main.Next.Next.RunContainer();

            //WriteLine();
            //WriteLine();
            //WriteLine();
            //WriteLine();
            //WriteLine();

            //int[,] arr = new int[5, 12];
            //WriteLine(arr.Length);
            //WriteLine(arr.GetLength(0));
            //WriteLine(arr.GetLength(1));

            string[] universe = { "galaxies", "stars", "planets", "sattelites" };
            RunMenu(universe, (0, 0));
        }

        static void DrawOptions(string[] options, (int x, int y) coordinates)
        {
            for(int i = 0; i < options.Length; i++)
            {
                if(i != coordinates.y)
                {
                    WriteLine(options[i]);
                }
                else
                {
                    ForegroundColor = ConsoleColor.Black;
                    BackgroundColor = ConsoleColor.White;
                    WriteLine(options[i]);
                    ResetColor();
                }
            }
        }
        static void RunMenu(string[] options, (int x, int y) coordinates)
        {
            //SetCursorPosition(coordinates.x, coordinates.y);
            ConsoleKey keyPressed;
            do
            {
                DrawOptions(options, coordinates);
                (int x, int y) currentCoordinates = GetCursorPosition();
                ConsoleKeyInfo keyInfo = ReadKey(true);
                keyPressed = keyInfo.Key;
                if(keyPressed == ConsoleKey.DownArrow)
                {
                    if(coordinates.y == options.Length - 1)
                    {
                        coordinates.y = 0;
                    }
                    else
                    {
                        coordinates.y++;
                    }
                }
                if (keyPressed == ConsoleKey.UpArrow)
                {
                    if (coordinates.y == 0)
                    {
                        coordinates.y = options.Length - 1;
                    }
                    else
                    {
                        coordinates.y--;
                    }
                }
                SetCursorPosition(coordinates.x, coordinates.y);
            } while (keyPressed != ConsoleKey.Enter);
        }
    }
}
