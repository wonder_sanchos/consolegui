﻿using System;
using System.Collections.Generic;
using static System.Console;

namespace MenuDemo
{
    public class Container
    {
        public Container Next { get; set; }
        private readonly string _containerName;
        private List<string> options = new List<string>();
        public int Index { get; set; }
        private int _selectedIndex = 0;
        private readonly Button[] _buttons;
        
        public Container(string name)
        {
            _containerName = name;
            Index = 0;
            _buttons = new Button[]
            {
                new Button(ButtonType.Exit, "Exit"),
                new Button(ButtonType.Back, "Back"),
                new Button(ButtonType.ToTop, "To Top")
            };
        }
        public void Add(string name)
        {
            Container newContainer = new Container(name);
            Container last = this;
            while(last.Next != null)
            {
                last = last.Next;
            }
            last.Next = newContainer;
            newContainer.Index = last.Index + 1;
        }
        private void CreateOptions()
        {
            options.Add(_containerName);
            for(int i = 0; i < _buttons.Length; i++)
            {
                if(Index < 1)
                {
                    if (_buttons[i].ButtonName == "Exit")
                    {
                        options.Add(_buttons[i].ButtonName);
                    }
                }
                else if(Index < 2)
                {
                    if (_buttons[i].ButtonName == "Back")
                    {
                        options.Add(_buttons[i].ButtonName);
                    }
                }
                else
                {
                    if (_buttons[i].ButtonName != "Exit")
                    {
                        options.Add(_buttons[i].ButtonName);
                    }
                }
            }
        }
        private void DisplayOptions()
        {
            if(options.Count == 0)
                CreateOptions();
            string currentOption;
            string prefix;
            for(int i = 0; i < options.Count; i++)
            {
                currentOption = $"<<{options[i]}>>";
                if (_selectedIndex == i)
                {
                    prefix = "*";
                    BackgroundColor = ConsoleColor.White;
                    ForegroundColor = ConsoleColor.Black;
                }
                else
                {
                    prefix = " ";
                    BackgroundColor = ConsoleColor.Black;
                    ForegroundColor = ConsoleColor.White;
                }
                WriteLine($"{prefix}{currentOption}");
            }
            ResetColor();
        }
        public int RunContainer()
        {
            ConsoleKey keyPressed;
            do
            {
                Clear();
                DisplayOptions();
                ConsoleKeyInfo keyInfo = ReadKey(true);
                keyPressed = keyInfo.Key;
                if(keyPressed == ConsoleKey.DownArrow)
                {
                    if(_selectedIndex == options.Count - 1)
                    {
                        _selectedIndex = 0;
                    }
                    else
                    {
                        _selectedIndex++;    
                    }
                }
                if (keyPressed == ConsoleKey.UpArrow)
                {
                    if (_selectedIndex == 0)
                    {
                        _selectedIndex = options.Count - 1;
                    }
                    else
                    {
                        _selectedIndex--;
                    }
                }

            } while (keyPressed != ConsoleKey.Enter);
            return -1;
        }
        public override string ToString()
        {
            return $"{_containerName}";
        }
    }
}
