﻿using System;
using static System.Console;

namespace MenuDemo
{
    public class Menu
    {
        private string[] _options;
        private int selectedIndex = 0;
        private Container _container;
        private Button[] _buttons;

        public Menu(Container container)
        {
            _container = container;
        }
        public Menu(string[] options)
        {
            _options = options;
        }
        
        private void DisplayMenu()
        {
            for(int i = 0; i < _options.Length; i++)
            {
                string currentOption = $"<< {_options[i]} >>";
                string prefix;
                if (i == selectedIndex)
                {
                    prefix = "*";
                    ForegroundColor = ConsoleColor.Black;
                    BackgroundColor = ConsoleColor.White;
                }
                else
                {
                    ForegroundColor = ConsoleColor.White;
                    BackgroundColor = ConsoleColor.Black;
                    prefix = " ";
                }
                WriteLine($"{prefix} {currentOption}");
            }
            ResetColor();
        }
        public string RunMenu()
        {
            ConsoleKey keyPressed;
            do
            {
                Clear();
                DisplayMenu();
                ConsoleKeyInfo keyInfo = ReadKey(true);
                keyPressed = keyInfo.Key;
                if (keyPressed == ConsoleKey.UpArrow)
                {
                    selectedIndex--;
                    if (selectedIndex < 0)
                    {
                        selectedIndex = _options.Length - 1;
                    }
                }
                else if (keyPressed == ConsoleKey.DownArrow)
                {
                    selectedIndex++;
                    if (selectedIndex > _options.Length - 1)
                    {
                        selectedIndex = 0;
                    }
                }
            } while (keyPressed != ConsoleKey.Enter);
            return _options[selectedIndex];
        }
    }
}
